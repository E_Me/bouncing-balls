Bouncing Balls

When the python code is executed, a small pygame window will appear showing 3 colorful balls bouncing automatically at the same time but with different speeds each ball. This process is looped until the user decides to exit.

Short video clip from the app:

![](Media/Dancing_Balls.mp4)
