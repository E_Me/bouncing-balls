# Multiple Bouncing Balls!

import pygame,sys,time,random
from pygame.locals import*
from sys import exit

class Ball:
    #Returns true if user clicks close
    def getExit():
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                exit()
                
    def main(self):
        pygame.init()

        #Window Parameters
        windowSize = (640,480)
        screen = pygame.display.set_mode(windowSize)
        surfaceColor = (0,0,0) #black color
        pygame.display.set_caption("Multiple Bouncing Balls!")

        #Ball Parameters
        speed1 = [3,4]
        speed2 = [3,2]
        speed3 = [2,3]
        
        b1 = {'color':(255,255,255),'x':50,'y':80,'radius':30}
        b2 = {'color':(255,0,0),'x':250,'y':180,'radius':30}
        b3 = {'color':(0,255,0),'x':550,'y':280,'radius':30}
        balls = [b1,b2,b3]
        
        clock = pygame.time.Clock()

        def movement():
            for b in balls:
                b1['x'] += speed1[0]
                b1['y'] += speed1[1]

                b2['x'] += speed2[0]
                b2['y'] += speed2[1]

                b3['x'] += speed3[0]
                b3['y'] += speed3[1]

        def out():
            for b in balls:
                if(b1['x']>(640 - b1['radius']) or (b1['x'] - b1['radius']) <= 0):
                    speed1[0] = -speed1[0]
                if(b1['y'] >(480 - b1['radius'])  or (b1['y'] - b1['radius']) <= 0):
                    speed1[1] = -speed1[1]

                if(b2['x']>(640 - b2['radius']) or (b2['x'] - b2['radius']) <= 0):
                    speed2[0] = -speed2[0]
                if(b2['y'] >(480 - b2['radius'])  or (b2['y'] - b2['radius']) <= 0):
                    speed2[1] = -speed2[1]

                if(b3['x']>(640 - b3['radius']) or (b3['x'] - b3['radius']) <= 0):
                    speed3[0] = -speed3[0]
                if(b3['y'] >(480 - b3['radius'])  or (b3['y'] - b3['radius']) <= 0):
                    speed3[1] = -speed3[1]
  
        #Looping the program
        while True:
            screen.fill(surfaceColor)

            for b in balls:
                pygame.draw.circle(screen,b['color'],(b['x'],b['y']),30)

            clock.tick(60)

            movement()
            out()
            
            pygame.display.update()
            Ball.getExit()
        
var = Ball()
var.main()
